# Create a string type data group called POOL_DG (or update for another name)
# For each record in teh data group, the string should be the hostname and the
# value should be the pool.
# The "equals" operand can be "equals", "starts_with", "ends_with", or "contains"

when HTTP_REQUEST {
    log local0. "Searching for [HTTP::host]"
    set matched_pool [class match -value [string tolower [HTTP::host]] equals POOL_DG]
    if { $matched_pool ne "" } {
        log local0. "Found match for [HTTP::host] with pool $matched_pool"
        pool $matched_pool
    } else {
        log local0. "No match found for [HTTP::host]"
        reject
    }
}