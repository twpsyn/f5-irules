# Create an address datagroup called "SNAT-Clients" where the addresses
# are the IPs (either individual or in CIDR notation) of the clients
# which should have SNAT applied to them (value fields aren't needed).
# All other clients will not have SNAT applied.

when CLIENT_ACCEPTED {
    if { [class match [IP::remote_addr] equals SNAT-Clients] } {
        log local0. "Client [IP::client_addr] SNAT matched"
        snat automap
    } else {
        log local0. "Client [IP::client_addr] SNAT not matched"
        snat none
    }
}