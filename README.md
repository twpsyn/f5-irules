# iRules

This is a collection of iRules that I have produced in my dev lab to test and prove concepts that would later be applied to production and customer F5s.

The iRules contained here can be used either wholly or as building blocks for new iRules.

All files have the .tcl extension to enable correct (TCL) syntax highlighting in capable editors.

## iRule descriptions

* __DG_Pool_Select_iRule.tcl__ - This irule uses a string type data group to identify the correct pool to be used for a particular URL.
* __HTTP_LOG.tcl__ - Quite extensive logging of HTTP traffic as it flows through the F5, noting the client IP, HTTP request and response headers, and the server selected to handle a connection.
* __SelectiveSNAT-iRule.tcl__ - Uses an address type data group to selectively enable SNAT based on client IP address.

### References
https://devcentral.f5.com/Wiki/iRules.HomePage.ashx - iRules Wiki Home